module.exports = {
    baseUrl: process.env.NODE_ENV === 'production' ?
        '/color-palette-analyzer' :
        '/'
}